package config

import (
    "os"
    "strconv"
)

type Config struct {
    APIKey      string
    Query       *QueryConfig
    Request     *RequestConfig
}

type QueryConfig struct {
    Symbol      string
    NDays       int
}

type RequestConfig struct {
    URL         string
    Function    string
}

func GetConfig() *Config {
    ndays, _ := strconv.Atoi(os.Getenv("NDAYS"))
    return &Config{
        APIKey: os.Getenv("APIKEY"),
        Query: &QueryConfig {
            Symbol: os.Getenv("SYMBOL"),
            NDays:  ndays,
        },
        Request: &RequestConfig {
            URL: "http://www.alphavantage.co",
            Function: "TIME_SERIES_DAILY_ADJUSTED",
        },
    }
}
