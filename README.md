# stock-app

## Installation
Install Kind as per docs: https://kind.sigs.k8s.io/docs/user/quick-start/

```
git clone git@gitlab.com:david_macneil/stock-app.git
cd stock-app/
kind create cluster --kubeconfig ~/.kube/kind --config kind-config.yaml
export KUBECONFIG=~/.kube/kind
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install ingress-nginx \
	ingress-nginx/ingress-nginx \
	--set controller.service.type=NodePort  \
	--set controller.service.nodePorts.http=30080  \
	--set controller.service.nodePorts.https=30443
kubectl create -f manifests/stock-app.yaml
```

## Testing
```
curl localhost:30080/stock
curl localhost:30080/stock?NDAYS=2
curl localhost:30080/stock?SYMBOL=AAP
curl localhost:30080/stock?NDAYS=2&SYMBOL=AAPL
```
