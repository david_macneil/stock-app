// https://github.com/mingrammer/go-todo-rest-api-example/
package main
import (
    //"fmt"
    "gitlab.com/david_macneil/stock-app/app"
    "gitlab.com/david_macneil/stock-app/config"
)


func main() {
    config := config.GetConfig()
    app :=  &app.App{}
    app.Initialise(config)
    app.Run(":8080")
}
