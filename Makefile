IMAGE ?= stock-app
VERSION ?= 0.0.2
DOCKERFILE ?= Dockerfile
NAME ?= stock-app
APP_PORT ?= 8080
LOCAL_PORT ?= 8080
REPO = damacneil

all: build run

build:
	docker build -t $(REPO)/$(IMAGE):$(VERSION) -f $(DOCKERFILE) .

run:
	docker rm -f $(NAME) || true
	docker run --name $(NAME) -d -p $(APP_PORT):$(LOCAL_PORT) -i -t $(REPO)/$(IMAGE):$(VERSION) 

push:
	docker push $(REPO)/$(IMAGE):$(VERSION)

exec:
	docker exec -i -t $(NAME) /bin/sh
