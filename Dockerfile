FROM golang:alpine as builder
RUN apk add git
RUN mkdir -p $GOPATH/src/gitlab.com/david_macneil/stock-app
COPY . $GOPATH/src/gitlab.com/david_macneil/stock-app
WORKDIR $GOPATH/src/gitlab.com/david_macneil/stock-app
RUN  go get ./... && go build

FROM alpine
RUN adduser -S -D -h /app stock-app
USER stock-app
COPY --from=builder /go/src/gitlab.com/david_macneil/stock-app/stock-app /app
WORKDIR /app/
CMD ["./stock-app"]
