package handler

import (
    "encoding/json"
    "log"
    "net/http"
    "strconv"

    "gitlab.com/david_macneil/stock-app/app/model"
)

func GetStock(w http.ResponseWriter, r *http.Request) {
    nDays := 0
    symbol := ""
    param := GetKey("NDAYS", r)
    n, err := strconv.Atoi(param); if err == nil {
        nDays = n
    } else {
        log.Printf("NDAYS is not a valid integer %s\n", param)
    }

    symbol = GetKey("SYMBOL", r)


    var stock = model.Stock{}
    (*model.Stock).GetStock(&stock, nDays, symbol)

    (*model.Stock).GetAverage(&stock)
    defer r.Body.Close()
    
    response, _ := json.Marshal(stock)
    w.Header().Set("Content-Type", "application/json")
    w.WriteHeader(http.StatusOK)
    w.Write([]byte(response))
}

func GetKey(key string, r *http.Request) string {
    keys, ok := r.URL.Query()[key]
    if !ok || len(keys[0]) < 1 {
        log.Printf("%s not specified, using default.\n", key)
        return ""
    }
    return keys[0]
}

