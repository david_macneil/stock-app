package app

import (
    "log"
    "net/http"
    "github.com/gorilla/mux"
    "gitlab.com/david_macneil/stock-app/app/model"
    "gitlab.com/david_macneil/stock-app/app/handler"
    "gitlab.com/david_macneil/stock-app/config"
)

type App struct {
    Router *mux.Router
}

func (a *App) Initialise(config *config.Config) {
    a.Router = mux.NewRouter()
    a.setRouters()
    stock := &model.Stock{}
    stock.Initialise(config)
}

func (a *App) setRouters() {
    a.Get("/stock", a.handleRequest(handler.GetStock))
}

func (a *App) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
    a.Router.HandleFunc(path, f).Methods("GET")
}

func (a *App) Run(host string) {
    log.Fatal(http.ListenAndServe(host, a.Router))
}

type RequestHandlerFunction func(w http.ResponseWriter, r *http.Request)

func (a *App) handleRequest(handler RequestHandlerFunction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		handler(w, r)
	}
}

