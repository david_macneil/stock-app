package model

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "log"
    "net/http"
    "sort"
    "strconv"

    "gitlab.com/david_macneil/stock-app/config"
)

type Stock struct {
    Data    []float32
    Average float32
}

var stockURI = ""
var nDays = 0
var symbol = ""

func (s *Stock) Initialise(config *config.Config) {
    stockURI = fmt.Sprintf("%s/query?apikey=%s&function=%s",
                    config.Request.URL,
                    config.APIKey,
                    config.Request.Function)
    nDays = config.Query.NDays
    symbol = config.Query.Symbol
    fmt.Println(stockURI)
}

func (s *Stock) GetStock(days int, sym string) {
    if days <= 0 {
        days = nDays
    }
    if sym == "" {
        sym = symbol
    }

    requestURL := fmt.Sprintf("%s&symbol=%s", stockURI, sym)
    log.Printf(requestURL)
    resp, err := http.Get(requestURL)
    if err != nil {
        log.Fatalln(err)
    }
    defer resp.Body.Close()

    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        log.Fatalln(err)
    }

    var result map[string]interface{}
    json.Unmarshal(body, &result)
    timeSeries := result["Time Series (Daily)"].(map[string]interface{})

    keys := make([]string, 0, len(timeSeries))
    for k := range timeSeries {
        keys = append(keys, k)
    }
    sort.Sort(sort.Reverse(sort.StringSlice(keys)))
    count := 0
    for _, k := range keys {
        daily := timeSeries[k].(map[string]interface{})
        closeValue, err := strconv.ParseFloat(daily["4. close"].(string), 32)
        if err != nil {
            log.Fatalln(err)
        }

        s.Data = append(s.Data, float32(closeValue))
        count ++
        if count == days {
            break
        }
    }
}

func (s *Stock) GetAverage() {
    var total float32 = 0
    for _, value:= range s.Data {
        total += value
    }
    s.Average = total/float32(len(s.Data))
}
